migrate:
	docker-compose run api python3 src/manage.py migrate $(if $m, api $m,)

makemigrations:
	docker-compose run api python3 src/manage.py makemigrations
	sudo chown -R ${USER} src/app/migrations/

createsuperuser:
	docker-compose run api python3 src/manage.py createsuperuser

collectstatic:
	docker-compose run api python3 src/manage.py collectstatic --no-input

dev:
	docker-compose run api python3 src/manage.py runserver localhost:8000

command:
	docker-compose run api python3 src/manage.py ${c}

shell:
	docker-compose run api python3 src/manage.py shell

debug:
	docker-compose run api python3 src/manage.py debug

piplock:
	docker-compose run api pipenv install
	sudo chown -R ${USER} Pipfile.lock

lint:
	docker-compose run api isort .
	docker-compose run api flake8 --config setup.cfg
	docker-compose run api black --config pyproject.toml .

check_lint:
	docker-compose run api isort --check --diff .
	docker-compose run api flake8 --config setup.cfg
	docker-compose run api black --config pyproject.toml .

runbot:
	python src/manage.py runbot

build:
	docker-compose build

up:
	docker-compose up -d

down:
	docker-compose down

push:
	docker-compose push

pull:
	docker-compose pull

test:
	docker-compose run -d nginx
	docker-compose run api pytest src/ -s
