import pytest
import requests

from app.internal.models.app_user import AppUser


@pytest.mark.django_db
@pytest.mark.smoke
async def test_databases_works():
    await AppUser.objects.acreate(id=2222, username='test', full_name='', phone_number='123123')
    user_db = await AppUser.objects.filter(id=2222).afirst()
    assert type(user_db) is AppUser
    assert user_db.id == 2222


@pytest.mark.django_db
@pytest.mark.smoke
async def test_web_app_return_200():
    res = requests.get('https://nooon.backend24.2tapp.cc/admin/')
    assert res.status_code == 200
