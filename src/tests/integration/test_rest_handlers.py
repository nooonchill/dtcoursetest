import sys

import pytest
from django.http import JsonResponse

from app.internal.transport.rest.handlers import endpoint_me


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.rest_handler
@pytest.mark.parametrize('user_key', ['user_0', 'user_max'])
async def test_correct_endpoint_me(django_db_setup, user_key):
    request = {'key': 'value'}
    user = django_db_setup[user_key]
    response = await endpoint_me(request, user.id)

    assert response.status_code == 200
    except_response_content = {
        "user":
        {
            "username": user.username,
            "telegram_id": user.id,
            "full_name": user.full_name,
            "phone": user.phone_number
        }
    }
    assert response.content == JsonResponse(except_response_content, status=200).content


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.rest_handler
@pytest.mark.parametrize('non_registered_user_id', [234, 32448])
async def test_non_registered_user_endpoint_me(django_db_setup, non_registered_user_id):
    request = {'key': 'value'}
    response = await endpoint_me(request, non_registered_user_id)

    assert response.status_code == 200
    except_response_content = {"user": {}}
    assert response.content == JsonResponse(except_response_content, status=200).content


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.rest_handler
@pytest.mark.parametrize('invalid_value_user_id', [-1, -sys.maxsize])
async def test_invalid_value_user_endpoint_me(django_db_setup, invalid_value_user_id):
    request = {'key': 'value'}
    with pytest.raises(ValueError):
        await endpoint_me(request, invalid_value_user_id)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.rest_handler
@pytest.mark.parametrize('invalid_type_user_id', ['', '1', [1], {'1': '1'}, {1}, (1, 2)])
async def test_invalid_type_user_endpoint_me(django_db_setup, invalid_type_user_id):
    request = {'key': 'value'}
    with pytest.raises(TypeError):
        await endpoint_me(request, invalid_type_user_id)
