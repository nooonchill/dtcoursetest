import sys
from decimal import Decimal
from unittest.mock import AsyncMock, MagicMock, Mock, patch

import pytest
import telegram
from telegram import Message, Update
from telegram.ext import ContextTypes

from app.internal.models.app_user import AppUser
from app.internal.models.bank_account import BankAccount
from app.internal.services.auth_service.auth_service import update_last_log_in_time
from app.internal.services.bank_service.bank_service import get_balance
from app.internal.transport.bot.handlers import (
    account_balance_command,
    card_balance_command,
    del_user_command,
    get_interacted_users_command,
    get_transactions_command,
    me_command,
    save_user_command,
    saved_users_command,
    set_phone_command,
    start_command,
    transfer_money_command,
)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.start_command
@pytest.mark.parametrize(
    ('id', 'first_name', 'last_name', 'username'),
    [
        (125, 'first', 'last', 'test'),
        (125, 'first_upd', 'last_upd', 'test_upd'),
        (125, 'first', '', 'test')
    ]
)
async def test_correct_start_command(update_context_mock, id, first_name, last_name, username):
    update, context = update_context_mock
    update.effective_user = telegram.User(
        id=id,
        first_name=first_name,
        last_name=last_name,
        is_bot=False,
        username=username
    )
    await start_command(update, context)
    excpected_reply = f'Данные пользователя {update.effective_user.full_name} сохранены (обновлены)!'
    update.message.reply_text.assert_called_once_with(excpected_reply)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.start_command
@pytest.mark.parametrize(
    ('id', 'first_name', 'last_name', 'username'),
    [
        (['abc'], [1], {'2': 'last'}, {'random_upd'}),
        ([1], {'1': 'first'}, {'last'}, ('random', 'upd')),
        ({'1': 0}, {'first'}, ('last', 'name'), ['random_upd']),
        ({1}, ('first', 'name'), ['last'], [1]),
        ((1, 2), ['first'], [2], {'username': 'random_upd'}),
        (True, True, True, {'username': True}),
        ('', '', '', '')
    ]
)
async def test_start_command_invalid_tg_user(update_context_mock, id, first_name, last_name, username):
    update, context = update_context_mock
    update.effective_user = telegram.User(
        id=id,
        first_name=first_name,
        last_name=last_name,
        is_bot=False,
        username=username
    )
    with pytest.raises(TypeError):
        await start_command(update, context)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.start_command
@pytest.mark.parametrize(('start_obj'), ['abc', '', 1, [1], {1}, {'1': 1}, True, None])
async def test_start_command_not_tg_user(update_context_mock, start_obj):
    update, context = update_context_mock
    update.effective_user = start_obj
    with pytest.raises(TypeError):
        await start_command(update, context)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.set_phone
@pytest.mark.parametrize(
    ('id', 'phone_number'),
    [
        (125, '+7953333'),
        (125, '1'),
        (125, '8' * 16)
    ]
)
async def test_correct_set_phone_command(update_context_mock, id, phone_number):
    update, context = update_context_mock
    update.effective_user = telegram.User(
        id=id,
        first_name='',
        is_bot=False,
    )
    context.args = [phone_number]
    await set_phone_command(update, context)
    excpected_reply = 'Номер телефона пользователя успешно сохранен!'
    edited_user = await AppUser.objects.filter(id=id).afirst()
    assert edited_user.phone_number == phone_number
    update.message.reply_text.assert_called_once_with(excpected_reply)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.set_phone
@pytest.mark.parametrize(('phone_number'), ['', '1' * 17, 'abc'])
async def test_set_invalid_value_phone_command(update_context_mock, phone_number):
    update, context = update_context_mock
    update.effective_user = telegram.User(
        id=0,
        first_name='',
        is_bot=False,
    )
    context.args = [phone_number]
    await set_phone_command(update, context)
    excpected_reply = 'Введены некорректные данные! Попробуйте еще раз!'
    update.message.reply_text.assert_called_once_with(excpected_reply)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.set_phone
@pytest.mark.parametrize(('phone_number'), [1, [1], {1}, {'1': 1}, True, None])
async def test_set_invalid_type_phone_command(update_context_mock, phone_number):
    update, context = update_context_mock
    update.effective_user = telegram.User(
        id=0,
        first_name='',
        is_bot=False,
    )
    context.args = [phone_number]
    await set_phone_command(update, context)
    excpected_reply = 'Введены некорректные данные! Попробуйте еще раз!'
    update.message.reply_text.assert_called_once_with(excpected_reply)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.me_command
@pytest.mark.parametrize('user_key', ['user_0', 'user_max'])
async def test_correct_me_command(update_context_mock, django_db_setup, user_key):
    update, context = update_context_mock
    id = django_db_setup[user_key].id
    update.effective_user = telegram.User(
        id=id,
        first_name='',
        is_bot=False,
    )
    context.user_data = {}
    await me_command(update, context)
    expected_full_name = django_db_setup[user_key].full_name
    expected_username = django_db_setup[user_key].username
    expected_phone_number = django_db_setup[user_key].phone_number
    excpected_reply = f'Информация о пользователе:\n\
ID: {id}\n\
Username: {expected_username}\n\
Полное имя: {expected_full_name}\n\
Номер телефона: {expected_phone_number}'
    update.message.reply_text.assert_called_once_with(excpected_reply)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.me_command
@pytest.mark.parametrize('user_key', ['user_random'])
async def test_no_phone_user_me_command(update_context_mock, django_db_setup, user_key):
    update, context = update_context_mock
    id = django_db_setup[user_key].id
    update.effective_user = telegram.User(
        id=id,
        first_name='',
        is_bot=False,
    )
    await me_command(update, context)
    excpected_reply = 'Невозможно использовать команду.\n\
Необходимо добавить номер телефона!\nИспользуйте /set_phone <phone_number>'
    update.message.reply_text.assert_called_once_with(excpected_reply)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.balance_command
@pytest.mark.parametrize('account_key', ['bank_account_0', 'bank_account_max'])
async def test_correct_account_balance_command(update_context_mock, django_db_setup, account_key):
    update, context = update_context_mock
    bank_account = django_db_setup[account_key]
    update.effective_user = telegram.User(
        id=0,
        first_name='',
        is_bot=False,
    )
    context.args = [bank_account.account]
    await account_balance_command(update, context)
    excpected_reply = f'Баланс банковского счета #{bank_account.account}: {await get_balance(bank_account)}'
    update.message.reply_text.assert_called_once_with(excpected_reply)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.balance_command
@pytest.mark.parametrize('account_number', ['1', '1' * 40])
async def test_invalid_value_account_balance_command(update_context_mock, account_number):
    update, context = update_context_mock
    update.effective_user = telegram.User(
        id=0,
        first_name='',
        is_bot=False,
    )
    context.args = [account_number]
    await account_balance_command(update, context)
    excpected_reply = 'Указан неверный банковский счет'
    update.message.reply_text.assert_called_once_with(excpected_reply)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.balance_command
@pytest.mark.parametrize('account_number', [1, [1], {1}, {'1': 1}, True, None])
async def test_invalid_type_account_balance_command(update_context_mock, account_number):
    update, context = update_context_mock
    update.effective_user = telegram.User(
        id=0,
        first_name='',
        is_bot=False,
    )
    context.args = [account_number]
    with pytest.raises(TypeError):
        await account_balance_command(update, context)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.balance_command
@pytest.mark.parametrize(
    ('bank_card_key'),
    [
        ('bank_card_0'),
        ('bank_card_max')
    ]
)
async def test_correct_card_balance_command(
    update_context_mock,
    django_db_setup,
    bank_card_key,
):
    update, context = update_context_mock
    update.effective_user = telegram.User(
        id=0,
        first_name='',
        is_bot=False,
    )
    bank_card = django_db_setup[bank_card_key]
    context.args = [bank_card.card_number]
    await card_balance_command(update, context)
    excpected_reply = f'Баланс банковской карты #{bank_card.card_number}: {await get_balance(bank_card)}'
    update.message.reply_text.assert_called_once_with(excpected_reply)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.balance_command
@pytest.mark.parametrize('card_number', ['', '1', '2' * 40])
async def test_invalid_value_card_balance_command(update_context_mock, card_number):
    update, context = update_context_mock
    update.effective_user = telegram.User(
        id=0,
        first_name='',
        is_bot=False,
    )
    context.args = [card_number]
    await card_balance_command(update, context)
    excpected_reply = 'Указан неверный номер банковской карты'
    update.message.reply_text.assert_called_once_with(excpected_reply)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.balance_command
@pytest.mark.parametrize('card_number', [1, [1], {1}, {'1': 1}, True, None])
async def test_invalid_type_card_balance_command(update_context_mock, card_number):
    update, context = update_context_mock
    update.effective_user = telegram.User(
        id=0,
        first_name='',
        is_bot=False,
    )
    context.args = [card_number]
    with pytest.raises(TypeError):
        await card_balance_command(update, context)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.transfer_money
@pytest.mark.parametrize(
    ('args', 'user_key', 'expected_source_key', 'expected_dest_key'),
    [
        (['0' * 20, '9' * 16, '200'], 'user_0', 'bank_account_0', 'bank_account_max'),
        (['0' * 16, '@' + 't' * 32, '1'], 'user_0', 'bank_account_0', 'bank_account_max'),
        (['@test_user_0', 'test_max', '1'], 'user_0', 'bank_account_0', 'bank_account_max'),
        (['@test_user_0', '9' * 20, '1'], 'user_0', 'bank_account_0', 'bank_account_max')
    ]
)
async def test_correct_transfer_money_command(
    update_context_mock,
    django_db_setup,
    args,
    user_key,
    expected_source_key,
    expected_dest_key
):
    update, context = update_context_mock
    user = django_db_setup[user_key]
    update.effective_user = telegram.User(
        id=user.id,
        first_name='',
        is_bot=False,
    )
    context.args = args
    await transfer_money_command(update, context)
    expected_source = django_db_setup[expected_source_key]
    expected_dest = django_db_setup[expected_dest_key]
    money_value = args[2]
    excpected_reply = f'Успешный перевод!\n\
Реквизиты отправителя: {expected_source}.\n\
Реквизиты получателя: {expected_dest}.\n\
Сумма: {float(money_value)}'
    update.message.reply_text.assert_called_once_with(excpected_reply)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.transfer_money
@pytest.mark.parametrize(
    ('args', 'user_key'),
    [
        (['0' * 20, '9' * 16, '1'], 'user_max'),
        (['0' * 16, '@' + 't' * 32, '1'], 'user_max'),
        (['@test_user_0', 'test_max', '1'], 'user_max'),
        (['@test_user_0', '9' * 20, '1'], 'user_max'),
    ]
)
async def test_invalid_user_transfer_money_command(update_context_mock, django_db_setup, args, user_key):
    user = django_db_setup[user_key]
    update, context = update_context_mock
    update.effective_user = telegram.User(
        id=user.id,
        first_name='',
        is_bot=False,
    )
    context.args = args
    with pytest.raises(TypeError):
        await transfer_money_command(update, context)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.transfer_money
@pytest.mark.parametrize(
    ('args', 'user_key'),
    [
        (['0' * 10, '9' * 15, '1'], 'user_0'),
        (['0' * 144, '@' + 't' * 12, '1'], 'user_0'),
        (['@abcd', 'test_invalid', '1'], 'user_0'),
        (['@abcd', '9' * 12, '1'], 'user_0'),
    ]
)
async def test_invalid_value_transfer_money_command(update_context_mock, django_db_setup, args, user_key):
    user = django_db_setup[user_key]
    update, context = update_context_mock
    update.effective_user = telegram.User(
        id=user.id,
        first_name='',
        is_bot=False,
    )
    context.args = args
    with pytest.raises(TypeError):
        await transfer_money_command(update, context)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.transfer_money
@pytest.mark.parametrize(
    ('args', 'user_key'),
    [
        ([1, 22, 333], 'user_0'),
        ([[1], [2, 2], [3, 3, 3]], 'user_0'),
        ([{'1': 1}, {'22': 22}, {'333': 333}], 'user_0'),
        ([(1), (2, 2), (3, 3, 3)], 'user_0'),
    ]
)
async def test_invalid_type_transfer_money_command(update_context_mock, django_db_setup, args, user_key):
    user = django_db_setup[user_key]
    update, context = update_context_mock
    update.effective_user = telegram.User(
        id=user.id,
        first_name='',
        is_bot=False,
    )
    context.args = args
    with pytest.raises(TypeError):
        await transfer_money_command(update, context)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.saved_user
@pytest.mark.parametrize(
    ('args', 'user_key'),
    [
        (['@random', 'rand'], 'user_0'),
        (['@random', 'rand_name'], 'user_max')
    ]
)
async def test_correct_save_user_command(update_context_mock, django_db_setup, args, user_key):
    user = django_db_setup[user_key]
    update, context = update_context_mock
    update.effective_user = telegram.User(
        id=user.id,
        first_name='',
        is_bot=False,
    )
    context.args = args
    await save_user_command(update, context)
    excpected_reply = 'Пользователь успешно добавлен в список сохранненых'
    update.message.reply_text.assert_called_once_with(excpected_reply)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.saved_user
@pytest.mark.parametrize(
    ('args', 'user_key'),
    [
        (['@', 'test'], 'user_0'),
        (['@dddddd', 'test'], 'user_max')
    ]
)
async def test_invalid_username_save_user_command(update_context_mock, django_db_setup, args, user_key):
    user = django_db_setup[user_key]
    update, context = update_context_mock
    update.effective_user = telegram.User(
        id=user.id,
        first_name='',
        is_bot=False,
    )
    context.args = args
    await save_user_command(update, context)
    excpected_reply = 'Информация о указанном пользователе не найдена'
    update.message.reply_text.assert_called_once_with(excpected_reply)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.saved_user
@pytest.mark.parametrize(
    ('args', 'user_key'),
    [
        ([1, 2], 'user_0'),
        ([[1], [2]], 'user_0'),
        ([{1}, {2}], 'user_0'),
        ([(1, 1), (2, 2)], 'user_0'),
        ([{'1': 1}, {'2': 2}], 'user_0')
    ]
)
async def test_invalid_args_save_user_command(update_context_mock, django_db_setup, args, user_key):
    user = django_db_setup[user_key]
    update, context = update_context_mock
    update.effective_user = telegram.User(
        id=user.id,
        first_name='',
        is_bot=False,
    )
    context.args = args
    with pytest.raises(TypeError):
        await save_user_command(update, context)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.saved_user
@pytest.mark.parametrize(
    ('args', 'user_key'),
    [
        (['rand'], 'user_0'),
        (['@random'], 'user_max')
    ]
)
async def test_correct_del_user_command(update_context_mock, django_db_setup, args, user_key):
    user = django_db_setup[user_key]
    update, context = update_context_mock
    update.effective_user = telegram.User(
        id=user.id,
        first_name='',
        is_bot=False,
    )
    context.args = args
    await del_user_command(update, context)
    excpected_reply = 'Пользователь удален из списка сохранненых'
    update.message.reply_text.assert_called_once_with(excpected_reply)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.saved_user
@pytest.mark.parametrize(
    ('args', 'user_key'),
    [
        ([1], 'user_0'),
        ([{1}], 'user_max'),
        ([{'1': 2}], 'user_0'),
        ([(1, 2)], 'user_max')
    ]
)
async def test_invalid_tag_del_user_command(update_context_mock, django_db_setup, args, user_key):
    user = django_db_setup[user_key]
    update, context = update_context_mock
    update.effective_user = telegram.User(
        id=user.id,
        first_name='',
        is_bot=False,
    )
    context.args = args
    with pytest.raises(TypeError):
        await del_user_command(update, context)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.saved_user
@pytest.mark.parametrize(('user_key', 'expected_saved_key'), [('user_0', 'saved_user_0')])
async def test_correct_saved_users_command(update_context_mock, django_db_setup, user_key, expected_saved_key):
    user = django_db_setup[user_key]
    update, context = update_context_mock
    update.effective_user = telegram.User(
        id=user.id,
        first_name='',
        is_bot=False,
    )
    await saved_users_command(update, context)
    excpected_reply = f'Сохраненные пользователи:\n@{django_db_setup[expected_saved_key].saved_username} \
- {django_db_setup[expected_saved_key].name}\n'
    update.message.reply_text.assert_called_once_with(excpected_reply)


@pytest.mark.django_db
@pytest.mark.integration
@pytest.mark.saved_user
@pytest.mark.parametrize(
    ('user_key', 'expected_interacted_key'),
    [
        ('user_0', 'user_max'),
        ('user_max', 'user_0')
    ]
)
async def test_correct_get_interacted_users_command(update_context_mock, django_db_setup, user_key, expected_interacted_key):
    user = django_db_setup[user_key]
    update, context = update_context_mock
    update.effective_user = telegram.User(
        id=user.id,
        first_name='',
        is_bot=False,
    )
    context.args = []
    await get_interacted_users_command(update, context)
    expected_username = django_db_setup[expected_interacted_key].username
    excpected_reply = f'Пользователи, с которыми вы взаимодействовали:\n@{expected_username}\n'
    update.message.reply_text.assert_called_once_with(excpected_reply)
