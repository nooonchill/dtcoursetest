import sys

import pytest
import telegram

from app.internal.models.app_user import AppUser
from app.internal.services.user_service.user_service import (
    save_user_info,
    set_user_phone_number,
    start_chat,
    try_get_user_by_id,
    try_get_user_by_username,
    update_user_info,
)


@pytest.mark.django_db
@pytest.mark.save_and_update_user
@pytest.mark.parametrize(
    ('id', 'first_name', 'last_name', 'username'),
    [
        (1001, 'first', 'last', 'test'),
        (5555, '', '', '')
    ]
)
async def test_save_correct_user_info(django_db_setup, id, first_name, last_name, username):
    tg_user = telegram.User(
        id=id,
        first_name=first_name,
        last_name=last_name,
        is_bot=False,
        username=username
    )
    saved_user = await save_user_info(tg_user)
    assert type(saved_user) is AppUser
    assert saved_user.id == tg_user.id
    assert saved_user.full_name == str(tg_user.full_name)
    assert saved_user.username == tg_user.username


@pytest.mark.django_db
@pytest.mark.save_and_update_user
@pytest.mark.parametrize(
    ('id', 'first_name', 'last_name', 'username'),
    [
        (['abc'], [1], {'2': 'last'}, {'random_upd'}),
        ([1], {'1': 'first'}, {'last'}, ('random', 'upd')),
        ({'1': 0}, {'first'}, ('last', 'name'), ['random_upd']),
        ({1}, ('first', 'name'), ['last'], [1]),
        ((1, 2), ['first'], [2], {'username': 'random_upd'}),
        (True, True, True, {'username': True}),
        ('', '', '', '')
    ]
)
async def test_save_invalid_type_user_info(django_db_setup, id, first_name, last_name, username):
    tg_user = telegram.User(
        id=id,
        first_name=first_name,
        last_name=last_name,
        is_bot=False,
        username=username
    )
    with pytest.raises(TypeError):
        await save_user_info(tg_user)


@pytest.mark.django_db
@pytest.mark.save_and_update_user
@pytest.mark.parametrize(('saved_obj'), ['abc', '', 1, [1], {1}, {'1': 1}, True, None])
async def test_save_not_tg_user(django_db_setup, saved_obj):
    with pytest.raises(TypeError):
        await save_user_info(saved_obj)


@pytest.mark.django_db
@pytest.mark.save_and_update_user
@pytest.mark.parametrize(
    ('id', 'first_name', 'last_name', 'username'),
    [
        (12345, 'first', 'last', 'random_upd'),
        (12345, '', '', 'random')
    ]
)
async def test_update_correct_user_info(django_db_setup, id, first_name, last_name, username):
    tg_user = telegram.User(
        id=id,
        first_name=first_name,
        last_name=last_name,
        is_bot=False,
        username=username
    )
    updated_user = await update_user_info(tg_user)
    assert type(updated_user) is AppUser
    assert updated_user.id == tg_user.id
    assert updated_user.full_name == str(tg_user.full_name)
    assert updated_user.username == str(tg_user.username)


@pytest.mark.django_db
@pytest.mark.save_and_update_user
@pytest.mark.parametrize(
    ('id', 'first_name', 'last_name', 'username'),
    [
        (['abc'], [1], {'2': 'last'}, {'random_upd'}),
        ([1], {'1': 'first'}, {'last'}, ('random', 'upd')),
        ({'1': 0}, {'first'}, ('last', 'name'), ['random_upd']),
        ({1}, ('first', 'name'), ['last'], [1]),
        ((1, 2), ['first'], [2], {'username': 'random_upd'}),
        (True, True, True, {'username': True}),
        ('', '', '', '')
    ]
)
async def test_update_invalid_type_user_info(django_db_setup, id, first_name, last_name, username):
    tg_user = telegram.User(
        id=id,
        first_name=first_name,
        last_name=last_name,
        is_bot=False,
        username=username
    )
    with pytest.raises(TypeError):
        await save_user_info(tg_user)


@pytest.mark.django_db
@pytest.mark.save_and_update_user
@pytest.mark.parametrize(('saved_obj'), ['abc', '', 1, [1], {1}, {'1': 1}, True, None])
async def test_update_not_tg_user(django_db_setup, saved_obj):
    with pytest.raises(TypeError):
        await update_user_info(saved_obj)


@pytest.mark.django_db
@pytest.mark.save_and_update_user
@pytest.mark.parametrize(
    ('id', 'first_name', 'last_name', 'username'),
    [
        (25, 'first', 'last', 'test'),
        (25, 'first_upd', 'last_upd', 'test_upd'),
        (25, 'first', 'last', 'test')
    ])
async def test_start_chat_correct_user_info(django_db_setup, id, first_name, last_name, username):
    tg_user = telegram.User(
        id=id,
        first_name=first_name,
        last_name=last_name,
        is_bot=False,
        username=username
    )
    started_user = await start_chat(tg_user)
    assert started_user.id == tg_user.id
    assert started_user.full_name == str(tg_user.full_name)
    assert started_user.username == str(tg_user.username)


@pytest.mark.django_db
@pytest.mark.save_and_update_user
@pytest.mark.parametrize(('saved_obj'), ['abc', '', 1, [1], {1}, {'1': 1}, True, None])
async def test_start_chat_not_tg_user(django_db_setup, saved_obj):
    with pytest.raises(TypeError):
        await start_chat(saved_obj)


@pytest.mark.django_db
@pytest.mark.save_and_update_user
@pytest.mark.parametrize(
    ('id', 'phone_number'),
    [
        ('user_0', '1'),
        ('user_random', '+79991234567'),
        ('user_max', '0' * 16)
    ]
)
async def test_set_user_correct_phone_number(django_db_setup, id, phone_number):
    user = django_db_setup[id]
    updated_user = await set_user_phone_number(user, phone_number)
    assert updated_user.id == user.id
    assert updated_user.phone_number == phone_number


@pytest.mark.django_db
@pytest.mark.save_and_update_user
@pytest.mark.parametrize(
    ('user_key', 'phone_number'),
    [
        ('user_0', ''),
        ('user_random', '-_?!.'),
        ('user_max', '0' * 17)
    ]
)
async def test_set_user_invalid_value_phone_number(django_db_setup, user_key, phone_number):
    user = django_db_setup[user_key]
    with pytest.raises(ValueError):
        await set_user_phone_number(user, phone_number)


@pytest.mark.django_db
@pytest.mark.save_and_update_user
@pytest.mark.parametrize(('updated_obj'), ['abc', '', 1, [1], {1}, {'1': 1}, True, None])
async def test_set_user_invalid_type_user_arg(django_db_setup, updated_obj):
    with pytest.raises(TypeError):
        await set_user_phone_number(updated_obj, '123')


@pytest.mark.django_db
@pytest.mark.save_and_update_user
@pytest.mark.parametrize('invalid_phone_number', [1, [1], {1}, {'1': 1}, True, None])
async def test_set_user_invalid_type_phone_number(django_db_setup, invalid_phone_number):
    user = django_db_setup['user_0']
    with pytest.raises(TypeError):
        await set_user_phone_number(user, invalid_phone_number)


@pytest.mark.django_db
@pytest.mark.get_user
@pytest.mark.parametrize(
    ('id', 'expected_user_key'),
    [
        (0, 'user_0'),
        (sys.maxsize, 'user_max'),
        (12345, 'user_random'),
    ]
)
async def test_get_user_by_correct_id(django_db_setup, id, expected_user_key):
    expected_user = django_db_setup[expected_user_key]
    user_got = await try_get_user_by_id(id)
    assert user_got == expected_user


@pytest.mark.django_db
@pytest.mark.get_user
@pytest.mark.parametrize('non_registred_id', [33, sys.maxsize - 1, 54321])
async def test_get_non_existed_user_by_id(django_db_setup, non_registred_id):
    user_got = await try_get_user_by_id(non_registred_id)
    assert user_got is None


@pytest.mark.django_db
@pytest.mark.get_user
@pytest.mark.parametrize('invalid_id', [-1, -sys.maxsize])
async def test_get_user_by_invalid_value_id(django_db_setup, invalid_id):
    with pytest.raises(ValueError):
        await try_get_user_by_id(invalid_id)


@pytest.mark.django_db
@pytest.mark.get_user
@pytest.mark.parametrize('invalid_id', ['abc', '', [1], {1}, {'1': 1}, True, None])
async def test_get_user_by_invalid_type_id(django_db_setup, invalid_id):
    with pytest.raises(TypeError):
        await try_get_user_by_id(invalid_id)


@pytest.mark.django_db
@pytest.mark.get_user
@pytest.mark.parametrize(
    ('username', 'expected_user_key'),
    [
        ('test_user_0', 'user_0'),
        ('t' * 32, 'user_max'),
        ('random', 'user_random'),
    ]
)
async def test_get_user_by_correct_username(django_db_setup, username, expected_user_key):
    user = django_db_setup[expected_user_key]
    user_got = await try_get_user_by_username(username)
    assert user_got == user


@pytest.mark.django_db
@pytest.mark.get_user
@pytest.mark.parametrize('non_registred_username', ['nothing', 'a' * 32])
async def test_get_non_existed_user_by_username(django_db_setup, non_registred_username):
    user_got = await try_get_user_by_username(non_registred_username)
    assert user_got is None


@pytest.mark.django_db
@pytest.mark.get_user
@pytest.mark.parametrize('invalid_username', [1, [1], {1}, {'1': 1}, True, None])
async def test_get_user_by_invalid_type_username(django_db_setup, invalid_username):
    with pytest.raises(TypeError):
        await try_get_user_by_username(invalid_username)
