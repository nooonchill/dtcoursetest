import sys
from decimal import Decimal

import pytest
import telegram
from django.db.models import Q

from app.internal.models.app_user import AppUser
from app.internal.models.bank_account import BankAccount
from app.internal.models.bank_card import BankCard
from app.internal.models.saved_user import SavedUser
from app.internal.models.transaction import Transaction
from app.internal.services.bank_service.bank_service import (
    get_interacted_users,
    get_transactions,
    try_get_bank_account,
    try_get_bank_card,
    try_get_transfer_details,
)


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.get_bank
@pytest.mark.parametrize(
    ('card_number', 'expected_card_key'),
    [
        ('0' * 16, 'bank_card_0'),
        ('9' * 16, 'bank_card_max'),
    ]
)
async def test_get_bank_card_by_correct_number(django_db_setup, card_number, expected_card_key):
    expected_card = django_db_setup[expected_card_key]
    bank_card = await try_get_bank_card(card_number)
    assert bank_card == expected_card


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.get_bank
@pytest.mark.parametrize('card_number', ['0', 'abcdef', '', '1' * 20])
async def test_get_bank_card_by_invalid_value_number(django_db_setup, card_number):
    bank_card = await try_get_bank_card(card_number)
    assert bank_card is None


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.get_bank
@pytest.mark.parametrize('non_registered_card_number', ['1234' * 4, '5' * 16])
async def test_get_non_registered_bank_card_by_number(django_db_setup, non_registered_card_number):
    bank_card = await try_get_bank_card(non_registered_card_number)
    assert bank_card is None


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.get_bank
@pytest.mark.parametrize('not_card_number', [1, [1], {1}, {'1': 1}, True, None])
async def test_get_bank_card_by_type_error_number(django_db_setup, not_card_number):
    with pytest.raises(TypeError):
        bank_card = await try_get_bank_card(not_card_number)
        assert bank_card is None


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.get_bank
@pytest.mark.parametrize(
    ('card_key', 'expected_account_key'),
    [
        ('bank_card_0', 'bank_account_0'),
        ('bank_card_max', 'bank_account_max')
    ]
)
async def test_get_bank_account_by_card(django_db_setup, card_key, expected_account_key):
    card_number = django_db_setup[card_key].card_number
    got_account = await try_get_bank_account(card_number)
    expected_account = django_db_setup[expected_account_key]
    assert got_account == expected_account


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.get_bank
@pytest.mark.parametrize(
    ('account, expected_account_key'),
    [
        ('0' * 20, 'bank_account_0'),
        ('9' * 20, 'bank_account_max')
    ]
)
async def test_try_get_bank_account_by_account_number(django_db_setup, account, expected_account_key):
    bank_account_got = await try_get_bank_account(account)
    expected_account = django_db_setup[expected_account_key]
    assert bank_account_got == expected_account


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.get_bank
@pytest.mark.parametrize(
    ('app_user_key, expected_account_key'),
    [
        ('user_0', 'bank_account_0'),
        ('user_max', 'bank_account_max')
    ]
)
async def test_try_get_bank_account_by_app_user(django_db_setup, app_user_key, expected_account_key):
    app_user = django_db_setup[app_user_key]
    bank_account_got = await try_get_bank_account(app_user)
    expected_account = django_db_setup[expected_account_key]
    assert bank_account_got == expected_account


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.get_bank
@pytest.mark.parametrize('object', [[1], {1}, {'1': 1}, True, None])
async def test_get_bank_account_type_error(django_db_setup, object):
    with pytest.raises(TypeError):
        await try_get_bank_account(object)


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.transaction
@pytest.mark.parametrize(
    ('args', 'app_user_id'),
    [
        (['0' * 20, '9' * 16, '234'], 0),
        (['0' * 16, '@' + 't' * 32, '463'], 0),
        (['@test_user_0', 'test_max', '542'], 0),
        (['@test_user_0', '9' * 20, '132'], 0),
        (['@test_user_0', 'test_max', '65'], 0),
    ]
)
async def test_get_correct_transfer_details(django_db_setup, args, app_user_id):
    app_user = await AppUser.objects.aget(id=app_user_id)
    transfer_details = await try_get_transfer_details(args, app_user)
    assert type(transfer_details['source']) is BankAccount
    assert type(transfer_details['destination']) is BankAccount
    assert transfer_details['value'] == float(args[2])
    assert transfer_details['user'] == app_user


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.transaction
@pytest.mark.parametrize(
    ('args', 'app_user_id'),
    [
        (['0' * 20, '0' * 20, '234'], 0),
        (['0' * 16, '0' * 16, '463'], 0),
        (['@test_user_0', '@test_user_0', '542'], 0),
        (['@test_user_0', '0' * 20, '123'], 0),
    ]
)
async def test_get_transfer_details_to_yourself(django_db_setup, args, app_user_id):
    app_user = await AppUser.objects.aget(id=app_user_id)
    transfer_details = await try_get_transfer_details(args, app_user)
    assert transfer_details is None


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.transaction
@pytest.mark.parametrize(
    ('args', 'app_user_id'),
    [
        (['0' * 20, '9' * 20, '234'], sys.maxsize),
        (['0' * 16, '9' * 16, '463'], sys.maxsize),
        (['@' + 't' * 32, '0' * 16, '234'], 0),
        (['test_max', '0' * 20, '463'], 0)
    ]
)
async def test_get_transfer_details_invalid_source(django_db_setup, args, app_user_id):
    app_user = await AppUser.objects.aget(id=app_user_id)
    transfer_details = await try_get_transfer_details(args, app_user)
    assert transfer_details is None


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.transaction
@pytest.mark.parametrize(
    ('args', 'app_user_id'),
    [
        (1, 2),
        ([1], [2]),
        ({1}, {2}),
        ({'1': 2}, {'2': 1}),
        (True, False),
        (None, None)
    ]
)
async def test_get_transfer_details_type_error(django_db_setup, args, app_user_id):
    with pytest.raises(TypeError):
        app_user = await AppUser.objects.filter(id=app_user_id).afirst()
        await try_get_transfer_details(args, app_user)


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.transaction
@pytest.mark.parametrize(
    ('account', 'expected_transactions'),
    [
        ('bank_account_0', ['trans_0', 'trans_max']),
        ('bank_account_max', ['trans_0', 'trans_max'])
    ]
)
async def test_correct_get_transactions(django_db_setup, account, expected_transactions):
    target = django_db_setup[account]
    transactions = await get_transactions(target)
    print(transactions)
    for expected_transactions_key in expected_transactions:
        expected_transaction = django_db_setup[expected_transactions_key]
        expected_result = {
            'source': expected_transaction.source.account,
            'destination': expected_transaction.destination.account,
            'value': Decimal(expected_transaction.value + 0.00),
            'created': expected_transaction.created,

        }
        assert expected_result in transactions


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.transaction
@pytest.mark.parametrize('object', [[1], {1}, {'1': 1}, True, None])
async def test_invalid_type_account_get_transactions(object):
    with pytest.raises(TypeError):
        await get_transactions(object)


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.transaction
@pytest.mark.parametrize('account', ['12345123451234512345', '01' * 10])
async def test_invalid_value_account_get_transactions(account):
    with pytest.raises(TypeError):
        account = await try_get_bank_account(account)
        await get_transactions(account)


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.transaction
@pytest.mark.parametrize(
    ('account', 'expected_user_key'),
    [
        ('bank_account_0', 'user_max'),
        ('bank_account_max', 'user_0')
    ]
)
async def test_correct_get_interacted_users(django_db_setup, account, expected_user_key):
    target = django_db_setup[account]
    usernames = await get_interacted_users(target)
    expected_user = django_db_setup[expected_user_key]
    expected_result = [{'username': expected_user.username}]
    assert usernames == expected_result


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.transaction
@pytest.mark.parametrize('object', [[1], {1}, {'1': 1}, True, None])
async def test_invalid_type_account_get_interacted_users(object):
    with pytest.raises(TypeError):
        await get_interacted_users(object)


@pytest.mark.django_db
@pytest.mark.unit
@pytest.mark.transaction
@pytest.mark.parametrize('account', ['12345123451234512345', '01' * 10])
async def test_invalid_value_account_get_interacted_users(account):
    with pytest.raises(TypeError):
        account = await try_get_bank_account(account)
        await get_interacted_users(account)
