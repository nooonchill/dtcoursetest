import sys
from datetime import datetime
from decimal import Decimal
from unittest.mock import AsyncMock, Mock, patch

import pytest
from django.db import connections
from pytz import timezone
from telegram import Message, Update
from telegram.ext import ContextTypes

from app.internal.models.app_user import AppUser
from app.internal.models.bank_account import BankAccount
from app.internal.models.bank_card import BankCard
from app.internal.models.saved_user import SavedUser
from app.internal.models.transaction import Transaction


@pytest.fixture(scope='function')
def update_context_mock():
    update = Mock(Update)
    context = Mock(ContextTypes.DEFAULT_TYPE)
    context.user_data = {}
    update.message = Mock(Message)
    update.message.reply_text = AsyncMock()
    yield update, context


@pytest.fixture(scope='session')
def django_db_setup(django_db_blocker):
    with django_db_blocker.unblock():
        connections['default'].creation.create_test_db()
        test_db = {}
        test_db['user_0'] = app_user(0, username='test_user_0', phone_number='+79998886543')
        test_db['user_max'] = app_user(sys.maxsize, username='t' * 32, phone_number='89998886543')
        test_db['user_phone'] = app_user(125, username='t' * 32, phone_number='89998886543')
        test_db['user_random'] = app_user(12345, username='random')
        test_db['bank_account_0'] = bank_account('00000000000000000000', test_db['user_0'])
        test_db['bank_account_max'] = bank_account('99999999999999999999', test_db['user_max'])
        test_db['bank_card_0'] = bank_card('0000000000000000', test_db['bank_account_0'])
        test_db['bank_card_max'] = bank_card('9999999999999999', test_db['bank_account_max'])
        test_db['saved_user_0'] = saved_user(test_db['user_0'], test_db['user_max'].username, name='test_max')
        test_db['saved_user_max'] = saved_user(test_db['user_max'], test_db['user_0'].username, name='test_0')
        test_db['trans_0'] = transaction(test_db['bank_account_0'], test_db['bank_account_max'], 11111111)
        test_db['trans_max'] = transaction(test_db['bank_account_max'], test_db['bank_account_0'], round(Decimal(sys.maxsize / 1000000)))
    yield test_db
    with django_db_blocker.unblock():
        connections['default'].close()


def app_user(id, username='', full_name='', phone_number=''):
    return AppUser.objects.create(
        id=id,
        username=username,
        full_name=full_name,
        phone_number=phone_number,
        last_login=datetime.now().replace(tzinfo=timezone('UTC'))
    )


def bank_account(account, app_user):
    return BankAccount.objects.create(account=account, app_user=app_user)


def bank_card(card_number, bank_account, valid_thru=None):
    return BankCard.objects.create(
        card_number=card_number,
        bank_account=bank_account,
        valid_thru=valid_thru
    )


def saved_user(user_id, saved_username, name):
    return SavedUser.objects.create(user_id=user_id, saved_username=saved_username, name=name)


def transaction(source, destination, value=0, created=datetime.now()):
    return Transaction.objects.create(source=source, destination=destination, value=value, created=created)
