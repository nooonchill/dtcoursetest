from django.urls import path

from app.internal.transport.rest.handlers import auth, endpoint_me, update_tokens

urlpatterns = [
    path("me/<int:id>", endpoint_me, name='api_me'),
    path("auth/", auth, name='auth'),
    path("update_tokens/", update_tokens)
]
