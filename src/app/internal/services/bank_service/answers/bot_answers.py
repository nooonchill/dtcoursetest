from datetime import datetime

from app.internal.models.bank_account import BankAccount
from app.internal.models.bank_card import BankCard


def bank_card_balance_message(bank_card: BankCard | None, balance: float) -> str:
    return f'Баланс банковской карты #{bank_card.card_number}: {balance}'


def invalid_card_number_message() -> str:
    return 'Указан неверный номер банковской карты'


def bank_account_balance_message(bank_account: BankAccount, balance: float) -> str:
    return f'Баланс банковского счета #{bank_account.account}: {balance}'


def invalid_account_message() -> str:
    return 'Указан неверный банковский счет'


def invalid_transfer_details_message() -> str:
    return 'Указаны неверные реквизиты для перевода'


def transfer_completed_message(transfer_details) -> str:
    return f"Успешный перевод!\nРеквизиты отправителя: {transfer_details['source']}.\nРеквизиты получателя: {transfer_details['destination']}.\nСумма: {transfer_details['value']}"


async def saved_users_message(saved_users: [dict]) -> str:
    message = "Сохраненные пользователи:\n"
    for user in saved_users:
        message += f"@{user['saved_username']} - {user['name']}\n"
    return message


def user_added_to_saved_message() -> str:
    return "Пользователь успешно добавлен в список сохранненых"


def user_deleted_saved_message() -> str:
    return "Пользователь удален из списка сохранненых"


def transactions_message(transactions: [dict]) -> str:
    message = "Выписка по счету:\n(Дата, Отправитель, Получатель, Сумма\n"
    for transaction in transactions:
        time = transaction['created'].strftime("%d.%B.%Y %H:%M")
        message += f"{time}:\n{transaction['source']}, \
{transaction['destination']}, {transaction['value']}\n"
    return message


def interacted_users_message(users: [dict]) -> str:
    message = "Пользователи, с которыми вы взаимодействовали:\n"
    for user in users:
        message += f"@{user['username']}\n"
    return message
