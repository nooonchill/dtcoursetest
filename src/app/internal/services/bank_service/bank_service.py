from decimal import Decimal
from typing import Any, Dict

import telegram
from asgiref.sync import sync_to_async
from django.db import transaction
from django.db.models import Case, DecimalField, F, Q, Sum, Value, When

from app.internal.models.app_user import AppUser
from app.internal.models.bank_account import BankAccount
from app.internal.models.bank_card import BankCard
from app.internal.models.saved_user import SavedUser
from app.internal.models.transaction import Transaction
from app.internal.services.user_service.user_service import try_get_user_by_username


async def try_get_bank_card(card_number: str) -> BankCard | None:
    if type(card_number) is not str:
        raise TypeError(card_number)
    try:
        bank_card = await BankCard.objects.aget(card_number=card_number)
    except:
        bank_card = None
    return bank_card


async def get_balance(target: BankAccount | BankCard) -> int:
    if type(target) is BankCard:
        target = await sync_to_async(lambda: target.bank_account)()

    transactions = Transaction.objects.filter(Q(source=target) | Q(destination=target))
    sums = (await transactions.aaggregate
            (
                source=Sum(
                    Case(
                        When(source=target, then='value'), default=0, output_field=DecimalField())),
                destination=Sum(
                    Case(
                        When(destination=target, then='value'), default=0, output_field=DecimalField()))
            )
            )
    return sums['destination'] - sums['source']


async def try_get_bank_account_by_card(card_number: str) -> BankAccount | None:
    card = await try_get_bank_card(card_number)
    if card is None:
        return None
    account = await sync_to_async(lambda: card.bank_account)()
    return account


async def try_get_bank_account_by_account_number(account: str) -> BankAccount | None:
    return await BankAccount.objects.filter(account=account).afirst()


async def try_get_bank_account_by_app_user(app_user: AppUser) -> BankAccount | None:
    if type(app_user) is not AppUser:
        return None
    return await BankAccount.objects.filter(app_user=app_user).afirst()


async def try_get_saved_user_by_name(app_user: AppUser, name: str):
    return await SavedUser.objects.filter(user_id=app_user, name=name).afirst()


async def try_get_bank_account(param: AppUser | str, app_user=None) -> BankAccount | None:
    if type(param) is not str and type(param) is not AppUser:
        raise TypeError(param)
    elif app_user is not None and type(app_user) is not AppUser:
        raise TypeError(app_user)
    elif type(param) is AppUser:
        return await try_get_bank_account_by_app_user(param)
    elif await try_get_bank_account_by_card(param) is not None:
        return await try_get_bank_account_by_card(param)
    elif await try_get_bank_account_by_account_number(param) is not None:
        return await try_get_bank_account_by_account_number(param)
    else:
        if param[0] != '@' and await try_get_saved_user_by_name(app_user, param) is not None:
            param = '@' + (await try_get_saved_user_by_name(app_user, param)).saved_username
        return await try_get_bank_account_by_app_user(await try_get_user_by_username(param[1:]))


async def create_transaction(transfer_details: dict) -> None:
    if await get_balance(transfer_details['source']) < transfer_details['value']:
        return
    await Transaction.objects.acreate(
        source=transfer_details['source'],
        destination=transfer_details['destination'],
        value=transfer_details['value']
    )


async def try_get_transfer_details(args: [str], user: AppUser) -> dict | None:
    if any(type(data) is not str for data in args):
        return None

    transfer_details = {'source': await try_get_bank_account(args[0], user),
                        'destination': await try_get_bank_account(args[1], user),
                        'value': float(args[2]),
                        'user': user}
    if (transfer_details['source'] is None
        or transfer_details['destination'] is None
        or transfer_details['source'] == transfer_details['destination']
        or (await sync_to_async(lambda: transfer_details['source'].app_user)()).id
            != transfer_details['user'].id):
        return None
    return transfer_details


async def get_saved_users(user: AppUser) -> [dict]:
    return await sync_to_async(
        lambda:
        list(SavedUser.objects
             .filter(user_id=user.id)
             .values('saved_username', 'name'))
    )()


async def save_user(user: AppUser, username: str, name: str) -> None:
    return await SavedUser.objects.acreate(
        user_id=user,
        saved_username=username[1:],
        name=name
    )


async def del_saved_user(user: AppUser, param: str) -> None:
    if type(user) is not AppUser or type(param) is not str:
        raise TypeError(user)
    await SavedUser.objects.filter(user_id=user, saved_username=param[1:]).adelete()
    await SavedUser.objects.filter(user_id=user, name=param).adelete()


async def get_transactions(target: BankAccount | BankCard) -> [dict]:
    if type(target) not in [BankAccount, BankCard]:
        raise TypeError(target)

    if type(target) is BankCard:
        target = await sync_to_async(lambda: target.bank_account)()

    return await sync_to_async(
        lambda:
        list(Transaction.objects
             .filter(Q(source=target) | Q(destination=target))
             .values('source', 'destination', 'value', 'created'))
    )()


async def get_interacted_users(target: BankAccount | BankCard) -> [dict]:
    if type(target) not in [BankAccount, BankCard]:
        raise TypeError(target)

    if type(target) is BankCard:
        target = await sync_to_async(lambda: target.bank_account)()
    account = target.account
    transaction_users = await sync_to_async(
        lambda:
        list(Transaction.objects
             .filter(Q(source=target) | Q(destination=target))
             .annotate(
                 username=Case(
                     When(Q(source=account), then=F('destination__app_user__username')),
                     default=F('source__app_user__username')
                 )
             )
             .values('username')
             .distinct()
             )
    )()
    return transaction_users
