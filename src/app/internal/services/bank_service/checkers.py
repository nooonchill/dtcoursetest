from asgiref.sync import sync_to_async

from app.internal.models.app_user import AppUser
from app.internal.models.bank_account import BankAccount
from app.internal.models.saved_user import SavedUser
from app.internal.services.bank_service.bank_service import get_balance
from app.internal.services.user_service.user_service import try_get_user_by_username


async def is_transfer_details_correct(transfer_details: dict) -> bool:
    return (transfer_details['source'] != transfer_details['destination']
            and (await sync_to_async(lambda: transfer_details['source'].app_user)()).id
            == transfer_details['user'].id
            and await get_balance(transfer_details['source']) >= transfer_details['value'] >= 0)


async def is_saved_user_param_correct(user: AppUser, args: [str]) -> bool:
    if len(args) != 2:
        return False
    username = args[0]
    return (type(await try_get_user_by_username(username[1:])) is AppUser
            or type(SavedUser.objects.filter(user_id=user, saved_username=username[1:])) is SavedUser)
