def passoword_not_set_message():
    return 'При первом использовании необходимо установить пароль, используйте /login <пароль>'


def user_not_login():
    return 'Необходимо войти в приложение, используйте /login <пароль>'


def password_set_message():
    return 'Пароль был успешно установлен'


def correct_log_in_message():
    return 'Вы успешно вошли'


def invalid_password_message():
    return 'Указан неверный пароль'
