from app.internal.models.app_user import AppUser


def output_user_info(user: AppUser) -> dict:
    if user is None:
        return {
            "user": {
            }
        }
    return {
        "user": {
            "username": user.username,
            "telegram_id": user.id,
            "full_name": user.full_name,
            "phone": user.phone_number,
        },
    }
