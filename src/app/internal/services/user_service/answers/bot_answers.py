from app.internal.models.app_user import AppUser


def user_saved_message(user: AppUser) -> str:
    return f'Данные пользователя {user.full_name} сохранены (обновлены)!'


def user_not_saved_message() -> str:
    return "Невозможно использовать команду.\nДля начала работы используйте команду /start"


def phone_saved_message() -> str:
    return 'Номер телефона пользователя успешно сохранен!'


def phone_not_saved_message() -> str:
    return "Невозможно использовать команду.\nНеобходимо добавить номер телефона!\n\
Используйте /set_phone <phone_number>"


def user_info_message(user: AppUser) -> str:
    return (f"Информация о пользователе:\nID: {user.id}\nUsername: {user.username}\nПолное имя: {user.full_name}\n\
Номер телефона: {user.phone_number}")


def invalid_input_message() -> str:
    return "Введены некорректные данные! Попробуйте еще раз!"


def invalid_user_message() -> str:
    return "Информация о указанном пользователе не найдена"
