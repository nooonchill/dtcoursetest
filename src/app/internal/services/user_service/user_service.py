import telegram

from app.internal.models.app_user import AppUser
from app.internal.models.issued_token import IssuedToken
from app.internal.services.user_service.checkers import is_phone_number_correct, is_user_correct
from config.settings import JWT_SECRET


async def start_chat(user: telegram.User) -> AppUser:
    if not is_user_correct(user):
        raise TypeError(user)
    if await try_get_user_by_id(user.id) is None:
        user = await save_user_info(user)
    else:
        user = await update_user_info(user)
    return user


async def update_user_info(user: telegram.User) -> AppUser:
    if not is_user_correct(user):
        raise TypeError(user)
    user_data = AppUser.objects.filter(id=user.id)
    await user_data.aupdate(
        id=user.id,
        username=user.username,
        full_name=user.full_name
    )
    return await AppUser.objects.aget(id=user.id)


async def save_user_info(user: telegram.User) -> AppUser:
    if not is_user_correct(user):
        raise TypeError(user)
    return await AppUser.objects.acreate(
        id=user.id,
        username=user.username,
        full_name=user.full_name,
        phone_number=''
    )


async def set_user_phone_number(user: AppUser, phone_number: str) -> bool:
    if type(user) is not AppUser or type(phone_number) is not str:
        raise TypeError(user)
    if not await is_phone_number_correct(phone_number):
        raise ValueError(user)
    await AppUser.objects.filter(id=user.id).aupdate(phone_number=phone_number)
    return await AppUser.objects.aget(id=user.id)


async def try_get_user_by_id(id: int) -> AppUser | None:
    if type(id) is not int:
        raise TypeError(id)
    if id < 0:
        raise ValueError
    try:
        user = await AppUser.objects.aget(id=id)
    except:
        user = None
    return user


async def try_get_user_by_username(username: str) -> AppUser | None:
    if type(username) is not str:
        raise TypeError(id)
    try:
        user = await AppUser.objects.aget(username=username)
    except:
        user = None
    return user
