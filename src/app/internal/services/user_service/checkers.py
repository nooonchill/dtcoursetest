import re

import telegram


async def is_phone_number_correct(phone_number: str) -> bool:
    return (type(phone_number) is str
            and 0 < len(phone_number) <= 16
            and re.search(r'^\+?[0-9]+$', phone_number) is not None)


def is_user_correct(user):
    if (type(user) is not telegram.User
        or type(user.id) is not int
        or type(user.username) is not str
        or type(user.full_name) is not str):
        return False
    return True
