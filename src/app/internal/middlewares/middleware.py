import jwt
from django.http import HttpResponse
from django.shortcuts import redirect

from app.internal.services.user_service.user_service import try_get_user_by_id
from config.settings import AUTHORIZED_URI_STARTS, JWT_SECRET


def JWTAuth(get_response):
    def middleware(request):
        if request.path.startswith(AUTHORIZED_URI_STARTS):
            try:
                token = request.COOKIES['access_token']
                playload = jwt.decode(token, JWT_SECRET, algorithms=["HS256"])
                user = try_get_user_by_id(playload['id'])
                if user is None:
                    return HttpResponse("invalid token", status_code=401)
            except:
                return HttpResponse("auth to open", status_code=401)
        response = get_response(request)
        return response
    return middleware
