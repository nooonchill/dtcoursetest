from telegram.ext import Application, CommandHandler

from app.internal.transport.bot.handlers import (
    account_balance_command,
    card_balance_command,
    del_user_command,
    get_interacted_users_command,
    get_transactions_command,
    login_command,
    me_command,
    save_user_command,
    saved_users_command,
    set_phone_command,
    start_command,
    transfer_money_command,
)
from config.settings import (
    TELEGRAM_BOT_TOKEN,
    WEBHOOK_IP,
    WEBHOOK_MAX_CONNECTIONS,
    WEBHOOK_PORT,
    WEBHOOK_URL,
    WEBHOOK_URL_PATH,
)


def run_bot() -> None:
    application = Application.builder().token(TELEGRAM_BOT_TOKEN).build()
    handlers = [CommandHandler("start", start_command),
                CommandHandler("set_phone", set_phone_command, has_args=True),
                CommandHandler("me", me_command),
                CommandHandler("account_balance", account_balance_command, has_args=True),
                CommandHandler("card_balance", card_balance_command, has_args=True),
                CommandHandler("transfer_money", transfer_money_command, has_args=True),
                CommandHandler("saved_users", saved_users_command),
                CommandHandler("save_user", save_user_command, has_args=True),
                CommandHandler('del_saved_user', del_user_command, has_args=True),
                CommandHandler('get_transactions', get_transactions_command),
                CommandHandler('get_interacted_users', get_interacted_users_command),
                CommandHandler('login', login_command, has_args=True)]
    application.add_handlers(handlers)
    application.run_webhook(
        listen=WEBHOOK_IP,
        port=WEBHOOK_PORT,
        url_path=WEBHOOK_URL_PATH,
        webhook_url=WEBHOOK_URL,
        max_connections=WEBHOOK_MAX_CONNECTIONS
    )


if __name__ == "__main__":
    run_bot()
