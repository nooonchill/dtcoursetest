from django.core.validators import MinLengthValidator
from django.db import models


class SavedUser(models.Model):
    user_id = models.ForeignKey('AppUser', on_delete=models.CASCADE)
    saved_username = models.CharField(max_length=32, validators=[MinLengthValidator(5)])
    name = models.CharField(max_length=256)

    class Meta:
        unique_together = (('user_id', 'saved_username'),)

    def __str__(self):
        return f"{self.user_id}: {self.saved_username}"
