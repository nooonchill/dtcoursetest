import datetime

from django.core.validators import MinValueValidator
from django.db import models


class AppUser(models.Model):
    id = models.BigIntegerField(primary_key=True, validators=[MinValueValidator(0)])
    username = models.CharField(max_length=32, null=False, default='')
    full_name = models.CharField(max_length=129, null=False, default='')
    phone_number = models.CharField(max_length=16, null=True)
    password = models.CharField(max_length=120, null=True)
    last_login = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.username
