from django.db import models

from app.internal.models.bank_account import BankAccount


class Transaction(models.Model):
    source = models.ForeignKey(BankAccount, on_delete=models.CASCADE, related_name='source1')
    destination = models.ForeignKey(BankAccount, on_delete=models.CASCADE, related_name='destination1')
    value = models.DecimalField(max_digits=19, decimal_places=2)
    created = models.DateTimeField(auto_now_add=True, editable=False)

    def __str__(self):
        return f"#{self.id}: {self.source} to {self.destination}"
