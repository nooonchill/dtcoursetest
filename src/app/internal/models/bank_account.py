from django.core.validators import MinLengthValidator
from django.db import models

from app.internal.models.app_user import AppUser


class BankAccount(models.Model):
    account = models.CharField(primary_key=True, max_length=20, validators=[MinLengthValidator(20)])
    app_user = models.ForeignKey(AppUser, on_delete=models.CASCADE, blank=True, default='')

    def __str__(self):
        return f"Счет #{self.account}"
