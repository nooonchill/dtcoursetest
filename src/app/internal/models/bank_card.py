from django.core.validators import MinLengthValidator
from django.db import models
from month.models import MonthField

from app.internal.models.bank_account import BankAccount


class BankCard(models.Model):
    card_number = models.CharField(primary_key=True, max_length=16, validators=[MinLengthValidator(16)])
    bank_account = models.ForeignKey(BankAccount, on_delete=models.CASCADE)
    valid_thru = MonthField(null=True, blank=True, )

    def __str__(self):
        return f"Карта #{self.card_number}"
