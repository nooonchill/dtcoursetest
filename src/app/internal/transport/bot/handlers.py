from datetime import datetime, timedelta

from asgiref.sync import sync_to_async
from pytz import timezone
from telegram import Update
from telegram.ext import ContextTypes

from app.internal.services.auth_service.answers.bot_answers import (
    correct_log_in_message,
    invalid_password_message,
    password_set_message,
)
from app.internal.services.auth_service.auth_service import check_password, set_password, update_last_log_in_time
from app.internal.services.bank_service.answers.bot_answers import (
    bank_account_balance_message,
    bank_card_balance_message,
    interacted_users_message,
    invalid_transfer_details_message,
    saved_users_message,
    transactions_message,
    transfer_completed_message,
    user_added_to_saved_message,
    user_deleted_saved_message,
)
from app.internal.services.bank_service.bank_service import (
    create_transaction,
    del_saved_user,
    get_balance,
    get_interacted_users,
    get_saved_users,
    get_transactions,
    save_user,
    try_get_bank_account,
    try_get_transfer_details,
)
from app.internal.services.bank_service.checkers import is_saved_user_param_correct, is_transfer_details_correct
from app.internal.services.user_service.answers.bot_answers import (
    invalid_input_message,
    invalid_user_message,
    phone_not_saved_message,
    phone_saved_message,
    user_info_message,
    user_saved_message,
)
from app.internal.services.user_service.checkers import is_phone_number_correct
from app.internal.services.user_service.user_service import set_user_phone_number, start_chat
from app.internal.transport.bot.decorator import get_data_to_context


async def start_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    user = await start_chat(update.effective_user)
    message = user_saved_message(user)
    await update.message.reply_text(message)


@get_data_to_context('user')
async def set_phone_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    phone_number = context.args[0]
    if not await is_phone_number_correct(phone_number):
        message = invalid_input_message()
    else:
        user = context.user_data.get('user')
        await set_user_phone_number(user, phone_number)
        message = phone_saved_message()
    await update.message.reply_text(message)


@get_data_to_context('user')
async def me_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    user = context.user_data.get('user')
    if user.phone_number == '':
        message = phone_not_saved_message()
    else:
        message = user_info_message(user)
    await update.message.reply_text(message)


@get_data_to_context('account')
async def account_balance_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    bank_account = context.user_data.get('account')
    if bank_account is not None:
        message = bank_account_balance_message(bank_account, await get_balance(bank_account))
        await update.message.reply_text(message)


@get_data_to_context('card')
async def card_balance_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    bank_card = context.user_data.get('card')
    if bank_card is not None:
        message = bank_card_balance_message(bank_card, await get_balance(bank_card))
        await update.message.reply_text(message)


@get_data_to_context('user')
async def transfer_money_command(update: Update, context: ContextTypes) -> None:
    transfer_details = await try_get_transfer_details(context.args, context.user_data.get('user'))
    if not await is_transfer_details_correct(transfer_details):
        message = invalid_transfer_details_message()
    else:
        await create_transaction(transfer_details)
        message = transfer_completed_message(transfer_details)
    await update.message.reply_text(message)


@get_data_to_context('user')
async def saved_users_command(update: Update, context: ContextTypes) -> None:
    saved_users = await get_saved_users(context.user_data.get('user'))
    message = await saved_users_message(saved_users)
    await update.message.reply_text(message)


@get_data_to_context('user')
async def save_user_command(update: Update, context: ContextTypes) -> None:
    if not await is_saved_user_param_correct(context.user_data.get('user'), context.args):
        message = invalid_user_message()
    else:
        await save_user(context.user_data.get('user'), context.args[0], context.args[1])
        message = user_added_to_saved_message()
    await update.message.reply_text(message)


@get_data_to_context('user')
async def del_user_command(update: Update, context: ContextTypes) -> None:
    await del_saved_user(context.user_data.get('user'), context.args[0])
    await update.message.reply_text(user_deleted_saved_message())


@get_data_to_context('user')
async def get_transactions_command(update: Update, context: ContextTypes) -> None:
    if len(context.args) == 0:
        bank_account = await try_get_bank_account(context.user_data.get('user'))
    else:
        bank_account = await try_get_bank_account(context.args[0])
    transactions = await get_transactions(bank_account)
    message = transactions_message(transactions)
    await update.message.reply_text(message)


@get_data_to_context('user')
async def get_interacted_users_command(update: Update, context: ContextTypes) -> None:
    if len(context.args) == 0:
        bank_account = await try_get_bank_account(context.user_data.get('user'))
    else:
        bank_account = await try_get_bank_account(context.args[0])
    usersnames = await get_interacted_users(bank_account)
    message = interacted_users_message(usersnames)
    await update.message.reply_text(message)


@get_data_to_context('user_not_login')
async def login_command(update: Update, context: ContextTypes) -> None:
    user = context.user_data.get('user')
    password = context.args[0]
    if user.password is None:
        await set_password(user.id, password)
        await update.message.reply_text(password_set_message())
        return
    correct_pass = await check_password(user, password)
    if correct_pass:
        await update_last_log_in_time(user.id)
        message = correct_log_in_message()
    else:
        message = invalid_password_message()
    await update.message.reply_text(message)
