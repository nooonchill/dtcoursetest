from datetime import datetime, timedelta
from functools import wraps

from pytz import timezone
from telegram import Update
from telegram.ext import ContextTypes

from app.internal.services.auth_service.answers.bot_answers import user_not_login
from app.internal.services.bank_service.answers.bot_answers import invalid_account_message, invalid_card_number_message
from app.internal.services.bank_service.bank_service import try_get_bank_account, try_get_bank_card
from app.internal.services.user_service.answers.bot_answers import user_not_saved_message
from app.internal.services.user_service.user_service import try_get_user_by_id


def get_data_to_context(key=''):
    def decorator(func):
        @wraps(func)
        async def wrapper(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
            match key:
                case 'user_not_login':
                    user = await try_get_user_by_id(update.effective_user.id)
                    if user is None:
                        await update.message.reply_text(user_not_saved_message)
                        return
                    context.user_data['user'] = user
                case 'user':
                    user = await try_get_user_by_id(update.effective_user.id)
                    if user is None:
                        await update.message.reply_text(user_not_saved_message)
                        return
                    elif datetime.now().replace(tzinfo=timezone('UTC')) - user.last_login > timedelta(days=1):
                        await update.message.reply_text(user_not_login())
                        return
                    context.user_data['user'] = user
                case 'account':
                    account = await try_get_bank_account(context.args[0])
                    if account is None:
                        await update.message.reply_text(invalid_account_message())
                        return
                    context.user_data['account'] = account
                case 'card':
                    card = await try_get_bank_card(context.args[0])
                    if card is None:
                        await update.message.reply_text(invalid_card_number_message())
                        return
                    context.user_data['card'] = card
            await func(update, context)

        return wrapper

    return decorator
