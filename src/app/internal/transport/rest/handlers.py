import jwt
from django.core.exceptions import ValidationError
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect, render

from app.internal.forms.log_in import LogInForm
from app.internal.models.issued_token import IssuedToken
from app.internal.services.auth_service.auth_service import add_tokens_cookie, check_password
from app.internal.services.user_service.answers.response_answers import output_user_info
from app.internal.services.user_service.user_service import try_get_user_by_id
from config.settings import JWT_SECRET


async def endpoint_me(request, id: int) -> JsonResponse:
    out = output_user_info(await try_get_user_by_id(id))
    return JsonResponse(out, status=200)


async def auth(request):
    if request.method == 'POST':
        log_in_form = LogInForm(request.POST)

        if log_in_form.is_valid():
            id = int(log_in_form.cleaned_data['id'])
            user = await try_get_user_by_id(id)
            if user is None:
                return HttpResponse("user not found", status_code=401)
            elif not await check_password(user, log_in_form.cleaned_data['password']):
                return HttpResponse("password error", status_code=401)
            else:
                response = redirect('api_me', id=id)
                response = await add_tokens_cookie(response, id)
                return response
    else:
        log_in_form = LogInForm()

    context = {'form': log_in_form}
    return render(request, 'auth/auth.html', context)


async def update_tokens(request) -> JsonResponse:
    try:
        token = request.COOKIES['refresh_token']
        playload = jwt.decode(token, JWT_SECRET, algorithms=["HS256"])
        issued_token = await IssuedToken.objects.filter(jti=playload['jti']).afirst()
    except:
        return redirect('auth', status_code=307)
    if issued_token is None:
        return HttpResponse("no refresh token", status_code=401)
    elif issued_token.revoked:
        return redirect('auth', status_code=307)
    user = await try_get_user_by_id(playload['id'])
    await IssuedToken.objects.filter(user=user).aupdate(revoked=True)
    response = redirect('api_me', id=playload['id'])
    response = await add_tokens_cookie(response, playload['id'])
    return response
