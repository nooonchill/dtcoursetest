# Generated by Django 5.0.3 on 2024-03-11 13:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_bankaccount_bankcard'),
    ]

    operations = [
        migrations.AddField(
            model_name='bankcard',
            name='bank_account',
            field=models.CharField(choices=[], default='', max_length=20),
        ),
    ]
