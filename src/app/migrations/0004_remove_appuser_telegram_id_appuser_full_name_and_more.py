# Generated by Django 5.0.2 on 2024-02-14 13:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_rename_user_appuser'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='appuser',
            name='telegram_id',
        ),
        migrations.AddField(
            model_name='appuser',
            name='full_name',
            field=models.CharField(default='', max_length=129),
        ),
        migrations.AddField(
            model_name='appuser',
            name='phone_number',
            field=models.CharField(max_length=16, null=True),
        ),
        migrations.AlterField(
            model_name='appuser',
            name='id',
            field=models.IntegerField(primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='appuser',
            name='username',
            field=models.CharField(default='', max_length=32),
        ),
    ]
