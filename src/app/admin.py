from django.contrib import admin

from app.internal.admin.admin_user import AdminUser
from app.internal.models.app_user import AppUser
from app.internal.models.bank_account import BankAccount
from app.internal.models.bank_card import BankCard
from app.internal.models.issued_token import IssuedToken
from app.internal.models.saved_user import SavedUser
from app.internal.models.transaction import Transaction

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"

admin.site.register(AppUser)
admin.site.register(BankAccount)
admin.site.register(BankCard)
admin.site.register(SavedUser)
admin.site.register(Transaction)
admin.site.register(IssuedToken)
